const
  // URL of API Request
  URL = 'http://coinmarketcap-nexuist.rhcloud.com/api/pivx',

  // PIVX logo
  IMG_URL = 'img/whiteLogo.png',

  // This is the div id we will target to display data
  TARGET_DIV = document.getElementById('pivx-ticker'),

  // Tips
  TIPS_ADDRESS = 'DK96maaWEGfwfw8EdzmHJJKyxEZttg211z',

  // Allows script to automatically update the results
  AUTO_UPDATE = true,

  // Update frequency (in milliseconds)
  UPDATE_FREQ = 500;

var timer,
  settingsjson =
  '{' +
    '"current" : {' +
    '"scale":"usd"},' +
    '"currencies" : [' +
    '{"scale":"usd"},' +
    '{"scale":"eur"},' +
    '{"scale":"cny"},' +
    '{"scale":"gbp"},' +
    '{"scale":"cad"},' +
    '{"scale":"rub"},' +
    '{"scale":"hkd"},' +
    '{"scale":"jpy"},' +
    '{"scale":"aud"},' +
    '{"scale":"brl"},' +
    '{"scale":"inr"},' +
    '{"scale":"krw"},' +
    '{"scale":"mxn"},' +
    '{"scale":"idr"},' +
    '{"scale":"chf"},' +
    '{"scale":"btc"}]' +
  '}',

  settings = JSON.parse(settingsjson);

// Update the ticker every 5 minutes
function updateTicker(data) {
  "use strict";
  // If it exists, clear the interval
  if (timer) {
    clearInterval(timer);
  }
  loadTicker(data);

  // If auto update is enabled...
  if (AUTO_UPDATE == true) {
    // Check timestamp
    var
      timestamp = +(data.timestamp);
      timer = setInterval(function() {
      // Get current time (in unix epoch format)
      var time = +((new Date()).getTime() / 1000);

      // If the current time is greater than
      // the timestamp time plus 5 minutes (300 seconds)
      if (time > (+timestamp + 300)) {
        // Request new data from API
        sendRequest();
      }
    }, UPDATE_FREQ);
  }
}

// loadTicker(JSON Object)
// loadTicker - This function will inject our data into the target div
// tickerData - The JSON object returned from our request to the API
function loadTicker(tickerData) {
  "use strict";

  // Get JSON Object from API request
  var
    data = tickerData,

    // Set data container class names
    classes = ['pivxTicker__logoWrapper', 'pivxTicker__statWrapper'],

    dataClasses = ['pivxTicker__settings','pivxTicker__price', 'pivxTicker__percent', 'pivxTicker__volume',  'pivxTicker__marketcap', 'pivxTicker__rank'];

    // If TARGET_DIV does not contain divs with the following classes then create them
    // item is the current value in the iteration of the array
    // index is the current index of the array
    function genesis(item, index) {
      if (!!(!TARGET_DIV.querySelector('.' + item))) {
        var el = document.createElement('div');
        TARGET_DIV.appendChild(el);
        TARGET_DIV.lastChild.setAttribute('class', item);
        if (index == 0) {
          var img = document.createElement('img'),
            tipDiv = document.createElement('div');
          TARGET_DIV.querySelector('.' + item).appendChild(img);
          TARGET_DIV.querySelector('.' + item).lastChild.setAttribute('class', 'pivxTicker__logo');
          TARGET_DIV.querySelector('.' + item).lastChild.setAttribute('src', IMG_URL);
          TARGET_DIV.querySelector('.' + item).lastChild.setAttribute('alt', 'PIVX Logo');
          // Add tip div
          TARGET_DIV.querySelector('.' + item).appendChild(tipDiv);
          TARGET_DIV.querySelector('.' + item).lastChild.setAttribute('class', 'pivxTicker__tip');
          TARGET_DIV.querySelector('.pivxTicker__tip').innerHTML = 'Created&nbsp;By:&nbsp;magg0t<br><a href="pivx:' + TIPS_ADDRESS + '">' + TIPS_ADDRESS + '</a>';
        }
      }
    }

    function subGenesis(item, index) {
      if (!!(!TARGET_DIV.querySelector('.' + item))) {
        var el = document.createElement('div');
        TARGET_DIV.querySelector('.pivxTicker__statWrapper').appendChild(el);
        TARGET_DIV.querySelector('.pivxTicker__statWrapper').lastChild.setAttribute('class', item);
      }
    }

    classes.forEach(genesis);
    dataClasses.forEach(subGenesis);

    TARGET_DIV.querySelector('.pivxTicker__settings').innerHTML = ('<div class="pivxTicker__settings--scale">Scale:&nbsp;<span>' + settings.current.scale + '</span></div>');
    TARGET_DIV.querySelector('.pivxTicker__price').innerHTML = ('<span class="lg-dollar">$</span>' + data.price.usd);
    // Determine if the % change is positive or negative and distribute classes based on the result
    if (data.change > 0) {
      TARGET_DIV.querySelector('.pivxTicker__percent').innerHTML = '<span class="caret">&utrif;</span>' + data.change + '<span class="sm-percent">%</span>';
      if (TARGET_DIV.querySelector('.pivxTicker__percent').classList.contains('red')) {
        TARGET_DIV.querySelector('.pivxTicker__percent').classList.remove('red');
      }
      TARGET_DIV.querySelector('.pivxTicker__percent').classList.add('green');
    } else {
      // Remove the '-' then display the number in red with a down caret
      var change = data.change;
      change = change.replace('-','');
      TARGET_DIV.querySelector('.pivxTicker__percent').innerHTML = '<span class="caret">&dtrif;</span>' + change + '<span class="sm-percent">%</span>';
      if (TARGET_DIV.querySelector('.pivxTicker__percent').classList.contains('green')) {
        TARGET_DIV.querySelector('.pivxTicker__percent').classList.remove('green');
      }
      TARGET_DIV.querySelector('.pivxTicker__percent').classList.add('red');
    }
    TARGET_DIV.querySelector('.pivxTicker__volume').innerHTML = '<span class="sm-dollar">$</span>' + (data.volume.usd).toLocaleString('decimal');
    TARGET_DIV.querySelector('.pivxTicker__marketcap').innerHTML = '<span class="sm-dollar">$</span>' + (data.market_cap.usd).toLocaleString('decimal');
    TARGET_DIV.querySelector('.pivxTicker__rank').innerHTML = data.position;
}


function sendRequest() {
  "use strict";
  var request = new XMLHttpRequest();
  request.open('GET', URL);
  request.onreadystatechange = function() {
    // If request finished and response is ready
    if (this.readyState == 4 && this.status == 200) {

      // set ticker equal to the JSON object returned from the request object
      var ticker = JSON.parse(request.responseText);

      updateTicker(ticker);
    }

    // If request status is in error let user know
    if (this.status != 200) {
      console.log('Sorry, the API returned an error. Please check back later.');
    }
  };

  // Send the request to the API
  request.send();
}
